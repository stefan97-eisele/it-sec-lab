function getCookie(name) {
    const cookies = document.cookie.split(';');
    for (let i = 0; i < cookies.length; i++) {
      const cookie = cookies[i].trim();
      if (cookie.startsWith(name + '=')) {
        return cookie.substring(name.length + 1);
      }
    }
    return '';
  }
  
  document.addEventListener('DOMContentLoaded', () => {
    const role = getCookie('role');
    if (role === 'admin') {
      document.getElementById('create-article-form').style.display = 'block';
    } else {
      document.getElementById('create-article-form').style.display = 'none';
    }

  
    document.getElementById('create-article-form').addEventListener('submit', (event) => {
      event.preventDefault();
      
      const title = document.getElementById('title').value;
      const content = document.getElementById('content').value;
      
      const xhr = new XMLHttpRequest();
      xhr.open('POST', '/create-article');
      xhr.setRequestHeader('Content-Type', 'application/json');
      xhr.onload = () => {
        if (xhr.status === 200) {
          alert('Article created successfully');
          window.location.href = '/article';
        } else {
          alert('Failed to create article');
        }
      };
      const requestBody = {
        title: title,
        content: content
      };
      xhr.send(JSON.stringify(requestBody));
    });
    

    document.getElementById('create-message-form').addEventListener('submit', (event) => {
      event.preventDefault();
      
      const name = document.getElementById('name').value;
      const content = document.getElementById('content').value;

      const xhr = new XMLHttpRequest();
      xhr.open('POST', '/add-message');
      xhr.setRequestHeader('Content-Type', 'application/json');
      xhr.onload = () => {
        if (xhr.status === 200) {
          alert('Article created successfully');
          window.location.href = '/message';
        } else {
          alert('Failed to create message');
        }
      };
      const requestBody = {
        name: name,
        content: content
      };
      xhr.send(JSON.stringify(requestBody));
    });
  
});
  
  