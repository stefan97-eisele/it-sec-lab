const generateRandomString = (length) => {
  const characters = '0123456789abcdefghijklmnopqrstuvwxyz';
  let randomString = '';
  for (let i = 0; i < length; i++) {
    const randomIndex = Math.floor(Math.random() * characters.length);
    randomString += characters.charAt(randomIndex);
  }
  return randomString;
};

const loginForm = document.getElementById('login-form');
loginForm.addEventListener('submit', async (event) => {
  event.preventDefault();
  const username = document.getElementById('username').value;
  const password = document.getElementById('password').value;
  const role = document.getElementById('role').value;

  const response = await fetch('/login', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      username: username,
      password: password,
      role: role
    })
  });

  const result = await response.text();
  if (result === 'success') {
    const sessionId = generateRandomString(40);
    // Set cookie for user's role
    document.cookie = `role=${role}`;            //Secure; HttpOnly`;
    document.cookie = `continueCode=${sessionId}`; //Secure; HttpOnly`;
    window.location.href = '/index.html'; // redirect to startpage if login is successful
  } else {
    alert(result); // show error message if login is not successful
  }
});
