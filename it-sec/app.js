const express = require('express');
const mysql = require('mysql2/promise');
const path = require('path');

const app = express();
const port = process.env.PORT || 3000;
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');


app.use(express.json());
app.use(express.static(path.join(__dirname, '/')));
app.use('/styles', express.static(__dirname + '/'));
app.use(cookieParser());
app.use(bodyParser.json());


const pool = mysql.createPool({
  host: 'localhost',
  user: 'root',
  password: 'root',
  database: 'it-sec',
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0
});

app.post('/login', async (req, res) => {
  try {
    const username = req.body.username;
    const password = req.body.password;
    const role = req.body.role;
  
    const query = `SELECT * FROM users WHERE username = '${username}' AND password = '${password}'`; // Für SQL injection Nutzername --> ' OR '1=1  .. Passwort --> ' OR '1=1
    const [rows, fields] = await pool.query(query);
  
    if (rows.length > 0) {
      const userRole = rows[0].role;
      if (userRole === role) {
        res.send('success');
      } else {
        res.send('error: invalid role');
      }
    } else {
      res.send('error: invalid username or password');
    }
  } catch (err) {
    console.error(err);
    res.status(500).send('Internal Server Error');
  }
}); 

app.post('/create-article', (req, res) => {
  const { title, content } = req.body;

  // MySQL query to insert the article data
  const sql = 'INSERT INTO article (title, content) VALUES (?, ?)';

  // Execute the query
  pool.query(sql, [title, content], (err, result) => {
    if (err) {
      console.error(err);
      res.status(500).send('Failed to create article');
      return;
    }

    res.status(200).send('Article created successfully');
  });
});

app.post('/add-message', (req, res) => {
  const { name, content } = req.body;

  // MySQL query to insert the article data
  const sql = 'INSERT INTO message (name, content) VALUES (?, ?)';

  // Execute the query
  pool.query(sql, [name, content], (err, result) => {
    if (err) {
      console.error(err);
      res.status(500).send('Failed to create message');
      return;
    }

    res.status(200).send('message created successfully');
  });
});

app.get('/login', (req, res) => {
  res.sendFile(path.join(__dirname, 'login.html'));
});

app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
